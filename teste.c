#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"


void loop(int processo){
	double i;

	// printf(1, "\n processo_%d -- Comecei \n", processo);

	for(i = 0; i < 50000; i++)
		i = i;
		//for(j=0; j < 5000; j++);

	// printf(1, "\n processo_%d -- Terminei \n", processo);
}

int main(void){
	int processo[3];


	processo[0] = fork(100);
	if(processo[0] == 0){
		loop(0);
		exit();
	}

	processo[1] = fork(50);
	if(processo[1] == 0){
		loop(1);
		exit();
	}

	processo[2] = fork(250);
	if(processo[2] == 0){
		loop(2);
		exit();
	}

	wait();
	wait();
	wait();

	exit();

return 0;
}
